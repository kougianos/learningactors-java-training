package kougianos.day2;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AccountantTest {

    // Used for checking System.print on console
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    private Calculator calc = new Calculator();
    private Accountant accountant = new Accountant(calc);

    @Test
    void testCalculate() {
        System.setOut(new PrintStream(outContent));


        // Check invalid operation input
        accountant.calculate("Whatever", 1, 2);
        assertEquals("Wrong operation input. Possible operations are [Addition, Subtraction, Multiplication, Division, Exponentiation, DivisionRepresentation, PrimeFactors]\n", outContent.toString());
        outContent.reset();

        // Check invalid numbers input
        accountant.calculate("Addition", -5, 100);
        assertEquals("Number inputs are invalid. They should be in range [1-100] and second number should be 0 in case of PrimeFactors operation\n", outContent.toString());
        outContent.reset();

        accountant.calculate("Addition", 1, 2);
        assertEquals("3\n", outContent.toString());
        outContent.reset();

        accountant.calculate("PrimeFactors", 28, 0);
        assertEquals("28 = 2*2*7\n", outContent.toString());
        outContent.reset();

        // Restore original output
        System.setOut(originalOut);

    }

    @Test
    void testCost() {
        Accountant accountant2 = new Accountant(calc);
        accountant2.calculate("DivisionRepresentation", 78, 3);
        // Invalid
        accountant2.calculate("PrimeFactors", 2, 3);
        // Invalid
        accountant2.calculate("Subtraction", 150, 4);
        accountant2.calculate("PrimeFactors", 28, 0);
        // Invalid
        accountant2.calculate("Whatever", 28, 0);

        assertEquals(5.5, accountant2.getTotalCost());
        accountant2.printCostAndReset();
        assertEquals(0, accountant2.getTotalCost());
    }

    @Test
    void testTotalOperations() {
        Accountant.setTotalOperations(0);
        Accountant accountant1 = new Accountant(calc);
        Accountant accountant2 = new Accountant(calc);

        accountant1.calculate("Addition", 4, 5);
        accountant1.calculate("Exponentiation", 2, 3);
        accountant2.calculate("DivisionRepresentation", 78, 3);
        // Invalid
        accountant2.calculate("PrimeFactors", 2, 3);
        // Invalid
        accountant2.calculate("Subtraction", 150, 4);
        accountant2.calculate("PrimeFactors", 28, 0);
        // Invalid
        accountant2.calculate("Whatever", 28, 0);

        assertEquals(4, Accountant.getTotalOperations());
    }

}
