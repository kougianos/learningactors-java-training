package kougianos.day2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculatorTest {

    private Calculator calc = new Calculator();

    @Test
    void exponentiationTest() {
        assertEquals(8, calc.exponentiation(2, 3));
        assertEquals(49, calc.exponentiation(-7, 2));
    }

    @Test
    void divisionVisualRepresentationTest(){
        assertEquals("10 = 1*7 + 3", calc.divisionVisualRepresentation(10, 7));
        assertEquals("67 = 11*6 + 1", calc.divisionVisualRepresentation(67, 6));
    }

    @Test
    void primeFactorsTest() {
        assertEquals("24 = 2*2*2*3", calc.primeFactors(24));
        assertEquals("56 = 2*2*2*7", calc.primeFactors(56));
    }

}
