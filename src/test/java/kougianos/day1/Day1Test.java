package kougianos.day1;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

class Day1Test {

    // Used for checking System.print on console
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Test
    void testOddOrEven() {
        assertEquals("Even", Day1.isNumberOddOrEven(10));
        assertEquals("Even", Day1.isNumberOddOrEven(0));
        assertEquals("Even", Day1.isNumberOddOrEven(-4));
        assertEquals("Even", Day1.isNumberOddOrEven((int) -4.0));
        assertEquals("Odd", Day1.isNumberOddOrEven(11299087));
        assertEquals("Odd", Day1.isNumberOddOrEven(-9));
        assertEquals("Odd", Day1.isNumberOddOrEven(11));
    }

    @Test
    void testFactorial() {
        assertEquals(1, Day1.factorial(0));
        assertEquals(24, Day1.factorial(4));
        assertEquals(120, Day1.factorial(5));
        assertEquals(720, Day1.factorial(6));
        assertEquals(5040, Day1.factorial(7));
        assertEquals(479001600, Day1.factorial(12));
    }

    @Test
    void testHarmonic() {
        assertEquals(1, Day1.harmonic(1));
        assertEquals(1.5, Day1.harmonic(2));
        assertEquals(1.8333333333333333, Day1.harmonic(3));
        assertEquals(2.083333333333333, Day1.harmonic(4));
        assertEquals(2.7178571428571425, Day1.harmonic(8));
        assertEquals(2.8289682539682537, Day1.harmonic(9));
        assertEquals(2.9289682539682538, Day1.harmonic(10));
    }

    @Test
    void testDivision() {
        System.setOut(new PrintStream(outContent));

        assertEquals(1, Day1.division(3, 3));
        assertEquals(1.5, Day1.division(3, 2));
        assertEquals(5, Day1.division(0.5, 0.1));
        assertEquals(-2, Day1.division(-10, 5.0));
        assertEquals(0, Day1.division(15, 0.0));

        // Check message has been written to console
        assertEquals("Division by 0 is not possible.", outContent.toString());

        // Restore original output
        System.setOut(originalOut);
    }

    @Test
    void testLeap() {
        assertTrue(Day1.isYearLeap(2020));
        assertTrue(Day1.isYearLeap(2096));
        assertTrue(Day1.isYearLeap(2000));
        assertFalse(Day1.isYearLeap(1999));
        assertFalse(Day1.isYearLeap(2200));
        assertFalse(Day1.isYearLeap(2100));
    }

    @Test
    void testPrimeTill() {
        System.setOut(new PrintStream(outContent));
        Day1.primeNumbersTill(100);

        assertEquals("2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 ",
                outContent.toString());

        // Restore original output
        System.setOut(originalOut);
    }

    @Test
    void testDivisionVisualRepresentation() {
        assertEquals("10 = 1*7 + 3", Day1.divisionVisualRepresentation(10, 7));
        assertEquals("96 = 10*9 + 6", Day1.divisionVisualRepresentation(96, 9));
        assertEquals("90 = 10*9", Day1.divisionVisualRepresentation(90, 9));
    }
}
