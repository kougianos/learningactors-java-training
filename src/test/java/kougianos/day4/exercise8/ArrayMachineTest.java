package kougianos.day4.exercise8;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class ArrayMachineTest {

    @Test
    void testSortNumbersDescending() {
        ArrayMachine machine = new ArrayMachine();

        Integer[] expectedResult = new Integer[]{66,5,3,1,-78};
        machine.setNumbers(new Integer[]{1,5,3,-78,66});
        machine.sortNumbersDescending();

        assertArrayEquals(expectedResult, machine.getNumbers());
    }
}
