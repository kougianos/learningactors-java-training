package kougianos.day4.exercise9;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ListMachineTest {

    @Test
    void testCalculateMinNumber() {
        ListMachine machine = new ListMachine();

        machine.setNumbers(Arrays.asList(5, 8, 3, 1, 78, 1234, -73, -877, 0));
        assertEquals(Integer.valueOf(-877), machine.calculateMinNumber(machine.getNumbers()));
    }
}
