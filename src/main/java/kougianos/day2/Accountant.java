package kougianos.day2;

import java.util.Arrays;
import java.util.List;

/**
 * Accountant class.
 */
public class Accountant {

    private double totalCost = 0.0;
    private static int totalOperations = 0;

    public double getTotalCost() {
        return this.totalCost;
    }

    public static void setTotalOperations(int totalOperations) {
        Accountant.totalOperations = totalOperations;
    }

    public static int getTotalOperations() {
        return totalOperations;
    }

    private static final List<String> possibleOperations = Arrays.asList(
            "Addition",
            "Subtraction",
            "Multiplication",
            "Division",
            "Exponentiation",
            "DivisionRepresentation",
            "PrimeFactors"
    );

    private Calculator calculator;

    public Accountant(Calculator calculator) {
        this.calculator = calculator;
    }

    /**
     * Public function that uses a calculator and performs given operation.
     */
    public void calculate(String operation, int n1, int n2) {
        if (!possibleOperations.contains(operation)) {
            System.out.print("Wrong operation input. Possible operations are " + possibleOperations + "\n");
            totalCost += 0.5;
            return;
        }

        if (!isInputValid(operation, n1, n2)) {
            System.out.print("Number inputs are invalid. They should be in range [1-100] and second number " +
                    "should be 0 in case of PrimeFactors operation" + "\n");
            totalCost += 0.5;
            return;
        }

        Object result = null;

        switch (operation) {
            case "Addition":
                result = calculator.addition(n1, n2);
                totalCost += 1.0;
                break;
            case "Subtraction":
                result = calculator.subtraction(n1, n2);
                totalCost += 1.0;
                break;
            case "Multiplication":
                result = calculator.multiplication(n1, n2);
                totalCost += 1.0;
                break;
            case "Division":
                result = calculator.division(n1, n2);
                totalCost += 1.0;
                break;
            case "Exponentiation":
                result = calculator.exponentiation(n1, n2);
                totalCost += 2.0;
                break;
            case "DivisionRepresentation":
                result = calculator.divisionVisualRepresentation(n1, n2);
                totalCost += 2.0;
                break;
            case "PrimeFactors":
                result = calculator.primeFactors(n1);
                totalCost += 2.0;
                break;
            default:
                // do nothing, code should never reach here
                break;
        }

        setTotalOperations(totalOperations + 1);
        System.out.print(result + "\n");

    }

    /**
     * This function prints the accountant's cost so far and then sets it back to 0.0
     */
    public void printCostAndReset() {
        System.out.print("The accountant's cost so far is " + totalCost +
                ". His cost has been reset.\n");
        totalCost = 0;
    }

    public static void printTotalOperations() {
        System.out.print("Total operations for all accountants: " + totalOperations + "\n");
    }

    /**
     * Private function that validates given input.
     */
    private boolean isInputValid(String operation, int n1, int n2) {

        if (operation.equals("PrimeFactors")) {
            return n2 == 0 && n1 >= 1 && n1 <= 100;
        } else {
            return n1 >= 1 && n1 <= 100 && n2 >= 1 && n2 <= 100;
        }

    }

}
