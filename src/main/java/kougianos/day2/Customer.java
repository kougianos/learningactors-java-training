package kougianos.day2;

public class Customer {

    public static void main(String[] args) {
        Calculator calc = new Calculator();

        Accountant accountant1 = new Accountant(calc);
        Accountant accountant2 = new Accountant(calc);

        accountant1.calculate("Addition", 4, 5);
        accountant1.calculate("Exponentiation", 2, 3);
        accountant2.calculate("DivisionRepresentation", 78, 3);
        // Invalid
        accountant2.calculate("PrimeFactors", 2, 3);
        // Invalid
        accountant2.calculate("Subtraction", 150, 4);
        accountant2.calculate("PrimeFactors", 28, 0);
        // Invalid
        accountant2.calculate("Whatever", 28, 0);

        accountant1.printCostAndReset();
        accountant2.printCostAndReset();

        Accountant.printTotalOperations();

        // Invalid
        accountant1.calculate("Subtraction", 15, 0);
        accountant1.calculate("Subtraction", 15, 5);
        accountant1.printCostAndReset();

        Accountant.printTotalOperations();

    }
}
