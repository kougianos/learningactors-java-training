package kougianos.day2;

/**
 * Calculator class.
 */
public class Calculator {

    public int addition(int x, int y) {
        return x + y;
    }

    public int subtraction(int x, int y) {
        return x - y;
    }

    public int multiplication(int x, int y) {
        return x * y;
    }

    public int division(int x, int y) {
        return x / y;
    }

    /**
     * Exponentiation function without using Math package.
     */
    public int exponentiation(int x, int y) {
        int result = x;
        for (int i = 1; i < y; i++) {
            result *= x;
        }
        return result;
    }

    /**
     * Given a number n and a divisor k, write the number n in the following representation:
     * n = k*<result of n/k> + modulo. For example for n = 10, k = 7 print“10 = 1*7 + 3”
     */
    public String divisionVisualRepresentation(int dividend, int divisor) {
        String result = dividend + " = " + dividend / divisor + "*" + divisor;
        int mod = dividend % divisor;
        return mod == 0 ? result : result + " + " + mod;
    }

    /**
     * Prime factor analysis of a number. Returns string of prime factors representation of a number.
     * Eg. 24 = 2*2*2*3, 56 = 2*2*2*7
     */
    public String primeFactors(int number) {
        StringBuilder result = new StringBuilder(number + " = ");

        for (int i = 2; i < number; i++) {
            while (number % i == 0) {
                result.append(i).append('*');
                number = number / i;
            }
        }
        if (number > 2) {
            result.append(number);
        }

        return result.toString();

    }
}
