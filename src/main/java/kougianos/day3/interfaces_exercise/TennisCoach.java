package kougianos.day3.interfaces_exercise;

public class TennisCoach implements Coachable {

    public TennisCoach() {
        System.out.println("Tennis Coach created!");
    }

    @Override
    public void warmup() {
        System.out.println("Tennis warmup: 25' jump rope.");
    }

    @Override
    public void training() {
        System.out.println("Tennis training: 1vs1 tennis game, first player to 2 sets wins.");

    }

    @Override
    public void recovery() {
        System.out.println("Tennis recovery: Tai Chi for 30'");
    }
}
