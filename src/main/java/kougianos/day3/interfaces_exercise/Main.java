package kougianos.day3.interfaces_exercise;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Welcome to kougianos training center! \n" +
                "Please choose your favorite sport and we will direct you to an expert coach!\n" +
                "Football\t -> press 1\n" +
                "Basketball\t -> press 2\n" +
                "Tennis\t\t -> press 3\n" +
                "Crossfit\t -> press 4\n" +
                "Exit\t\t -> press 0\n");

        while (true) {
            int choice = scanner.nextInt();

            Coachable coach = null;
            switch (choice) {
                case 0:
                    System.out.println("Thank you for visiting our training center! Exiting program.");
                    return;
                case 1:
                    coach = new FootballCoach();
                    break;
                case 2:
                    coach = new BasketballCoach();
                    break;
                case 3:
                    coach = new TennisCoach();
                    break;
                case 4:
                    coach = new CrossfitCoach();
                    break;
                default:
                    System.out.println("Invalid input! Please choose a number in range [1-4] to train or 0 to exit.");
            }

            if (coach != null) {
                coach.executeAllMethods();
                System.out.println("Wanna train some more? Choose another sport or press 0 to exit program!");
            }
        }

    }
}
