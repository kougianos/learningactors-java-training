package kougianos.day3.interfaces_exercise;

public class CrossfitCoach implements Coachable {

    public CrossfitCoach() {
        System.out.println("Crossfit Coach created!");
    }

    @Override
    public void warmup() {
        System.out.println("Crossfit warmup: 99 push ups * 40 sets :)");
    }

    @Override
    public void training() {
        System.out.println("Crossfit training: Squat at least 250kgs, Bench press 300kgs and Deadlift half a ton.");

    }

    @Override
    public void recovery() {
        System.out.println("Crossfit recovery: 300' swimming against a river.");
    }
}
