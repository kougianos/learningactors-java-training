package kougianos.day3.interfaces_exercise;

public class FootballCoach implements Coachable {

    public FootballCoach() {
        System.out.println("Football Coach created!");
    }

    @Override
    public void warmup() {
        System.out.println("Football warmup: 25' aerobic running.");
    }

    @Override
    public void training() {
        System.out.println("Football training: 11vs11 football game, first team to score 3 goals wins.");

    }

    @Override
    public void recovery() {
        System.out.println("Football recovery: Myofascial release with a foam roller.");
    }
}
