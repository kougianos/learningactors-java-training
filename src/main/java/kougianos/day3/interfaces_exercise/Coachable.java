package kougianos.day3.interfaces_exercise;

/**
 * The Coach interface feature signatures for warmup, training, and recovery methods.
 */
public interface Coachable {
    void warmup();
    void training();
    void recovery();

    default void executeAllMethods() {
        warmup();
        training();
        recovery();
    }
}
