package kougianos.day3.interfaces_exercise;

public class BasketballCoach implements Coachable {

    public BasketballCoach() {
        System.out.println("Basketball Coach created!");
    }

    @Override
    public void warmup() {
        System.out.println("Basketball warmup: 10 full court sprints and 50 jump shots");
    }

    @Override
    public void training() {
        System.out.println("Football training: 5vs5 basketball game, first team to score 40 points wins.");

    }

    @Override
    public void recovery() {
        System.out.println("Basketball recovery: Hip and core activation exercises for 15'.");
    }
}
