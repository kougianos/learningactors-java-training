package kougianos.day3.inheritance_example;

public class Square extends TwoDimensionShape {

    public Square(double area) {
        super(area);
        isConvex = true;
        System.out.println("2D Shape Square created! Square is a convex shape");
    }

    @Override
    public void printArea() {
        System.out.println("Square's area is " + this.area);
    }

}
