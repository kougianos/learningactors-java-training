package kougianos.day3.inheritance_example;

public class Cube extends ThreeDimensionShape {

    public Cube(double area) {
        super(area);
        System.out.println("3D Shape Cube created!");
    }

    @Override
    public void printArea() {
        System.out.println("Cube's area is " + this.area);
    }

}
