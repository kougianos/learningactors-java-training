package kougianos.day3.inheritance_example;

public abstract class ThreeDimensionShape extends Shape {

    protected ThreeDimensionShape(double area) {
        super(area);
    }

}
