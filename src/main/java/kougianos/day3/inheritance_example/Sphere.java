package kougianos.day3.inheritance_example;

public class Sphere extends ThreeDimensionShape {

    public Sphere(double area) {
        super(area);
        System.out.println("3D Shape Sphere created!");
    }

    @Override
    public void printArea() {
        System.out.println("Sphere's area is " + this.area);
    }

}
