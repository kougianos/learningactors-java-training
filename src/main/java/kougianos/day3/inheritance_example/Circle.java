package kougianos.day3.inheritance_example;

public class Circle extends TwoDimensionShape {

    public Circle(double area) {
        super(area);
        isConvex = true;
        System.out.println("2D Shape Circle created! Circle is a convex shape");
    }

    @Override
    public void printArea() {
        System.out.println("Circle's area is " + this.area);
    }

}
