package kougianos.day3.inheritance_example;

public class Main {

    public static void main(String[] args) {
        Shape circle = new Circle(15);
        circle.printArea();

        Shape square = new Square(25.7);
        square.printArea();

        Shape sphere = new Sphere(65.787);
        sphere.printArea();

        Shape cube = new Cube(55.9);
        cube.printArea();
    }
}
