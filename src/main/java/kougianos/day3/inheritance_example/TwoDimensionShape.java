package kougianos.day3.inheritance_example;

public abstract class TwoDimensionShape extends Shape {

    protected boolean isConvex;

    protected TwoDimensionShape(double area) {
        super(area);
    }

    // Not needed for this example, but creating getter for private/protected attribute is best practice.
    public boolean isConvex() {
        return isConvex;
    }

}
