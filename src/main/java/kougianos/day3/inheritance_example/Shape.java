package kougianos.day3.inheritance_example;

public abstract class Shape {

    protected final double area;

    protected Shape(double area) {
        this.area = area;
    }

    public abstract void printArea();
}
