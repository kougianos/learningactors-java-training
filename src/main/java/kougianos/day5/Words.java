package kougianos.day5;

import java.util.*;
import java.util.stream.Collectors;

public class Words {
    public static void main(String[] args) {
        //Φέρε το κείμενο ως ένα Array απο String
        String[] words = getWords();
        //Τύπωσε ποιο είναι το μήκος της κάθε λέξης του κειμένου.
        countWordsLength(words);
        //Τύπωσε πόσες φορές εμφανίζεται η κάθε λέξη στο κείμενο.
        countWordsOccurrences(words);
        //Τύπωσε τη μεγαλυτερη σε μήκος λέξη και το μήκος της.
        findLongestWord(words);
        //Τύπωσε τη λέξη που εμφανίζεται περισσότερες φορές στο κείμενο και πόσες είναι.
        findMostOccurredWord(words);
        //Τύπωσε τις διακριτές λέξεις που εμφανίζονται στο κείμενο
        // καθώς και τις διπλότυπες με τη σειρά εμφάνισης τους
        countDistinctAndPrintNonDistinctWords(words);
        //Τύπωσε τις διακριτές λέξεις που εμφανίζονται στο κείμενο με λεξικογραφική (φυσική) σειρά
        printDistinctWordsInLexicographicOrder(words);
        //Τύπωσε τις διακριτές λέξεις με τη σειρά που εμφανίζονται στο κείμενο
        // αφου αφαιρεσεις (φιλτραρεις) οσες εμφανίζονται ακριβώς 3 φορές
        removeWordsThatOccurExactlyThreeTimes(words);
        //Τύπωσε με λεξικογραφική (φυσική) σειρά τις διακριτές λέξεις που εμφανίζονται στο κείμενο
        // αφου αφαιρεσεις (φιλτραρεις) οσες εχουν μήκος μεγαλύτερο απο 3 γράμματα
        removeWordsLongerThanThreeLetters(words);
    }

    private static String[] getWords() {
        String jfk = "We choose to go to the moon. " +
                "We choose to go to the moon in this decade and do the other things, " +
                "not because they are easy, " +
                "but because they are hard, " +
                "because that goal will serve to organize and measure the best of our energies and skills, " +
                "because that challenge is one that we are willing to accept, " +
                "one we are unwilling to postpone, " +
                "and one which we intend to win, " +
                "and the others, too.";
        return jfk
                .toLowerCase()
                .replace(".", "")
                .replace(",", "")
                .split(" ");
    }

    /**
     * Prints word length for each word, for String[] input. Insertion or lexicographical order doesn't matter,
     * so HashSet implementation is used.
     *
     * @param words input String[]
     */
    private static void countWordsLength(String[] words) {
        System.out.println("Word length for each word: ");
        Set<String> s = new HashSet<>(Arrays.asList(words));
        s.forEach(w -> System.out.println("\"" + w + "\"" + " length: " + w.length()));

        // Alternative solution, creating set:
//        Set<String> answer = Arrays.stream(words)
//                .map(w -> "\"" + w + "\"" + " length: " + w.length())
//                .collect(Collectors.toSet());
//        System.out.println(answer);
    }

    /**
     * Prints word occurrences for String[] input.
     *
     * @param words input String[]
     */
    private static void countWordsOccurrences(String[] words) {
        System.out.println("\nWord occurences for each word:");
        Arrays.stream(words)
                .collect(Collectors.groupingBy(w -> w))
                .forEach((k, v) -> System.out.println("\"" + k + "\"" + " occurrences: " + v.size()));

        // Alternative solution
//        List<String> l = Arrays.asList(words);
//        Set<String> s = new HashSet<>(l);
//        s.stream()
//                .forEach(e -> System.out.println("\"" + e + "\"" + " occurrences: " + Collections.frequency(l, e)));
    }

    /**
     * Prints the longest String in an array of strings, along with its length.
     * If longest String is not present, return empty string.
     *
     * @param words input String[]
     */
    private static void findLongestWord(String[] words) {
        String longest = Arrays.stream(words).
                max(Comparator.comparingInt(String::length)).orElse("");

        System.out.println("\nLongest string is " + "\"" + longest + "\"" + " with length: " + longest.length());
    }

    /**
     * Prints the word with the most occurrences for String[] input, and the occurrences of this word.
     *
     * @param words input String[]
     */
    private static void findMostOccurredWord(String[] words) {
        Map<String, Integer> m = getMapWithWordsAndOccurrences(words);

        Optional<Map.Entry<String, Integer>> maxEntry = m.entrySet()
                .stream()
                .max(Map.Entry.comparingByValue());

        System.out.println("\nWord with most occurrences:");
        maxEntry.ifPresent(entry -> System.out.println("\"" + entry.getKey() + "\": " + entry.getValue()));
    }

    /**
     * Prints distinct and duplicate words (with their appearance order) for input String[].
     *
     * @param words input String[]
     */
    private static void countDistinctAndPrintNonDistinctWords(String[] words) {
        Map<String, Integer> m = getMapWithWordsAndOccurrences(words);
        System.out.println("Distinct words:");
        m.forEach((key, value) -> System.out.println(key));

        System.out.println("\nDuplicate words, preserving appearance order: ");
        m.entrySet().stream()
                .filter(e -> e.getValue() > 1)
                .forEach(e -> System.out.println(e.getKey()));
    }

    /**
     * Prints distinct words in lexicographical order, for input String[].
     *
     * @param words input String[]
     */
    private static void printDistinctWordsInLexicographicOrder(String[] words) {
        System.out.println("\nDistinct words in lexicographical order: ");
        Set<String> s = new TreeSet<>(Arrays.asList(words));
        s.forEach(System.out::println);
    }

    /**
     * Removes words that occur exactly 3 times for input String[], then prints rest of the words preserving appearance
     * order.
     *
     * @param words input String[]
     */
    private static void removeWordsThatOccurExactlyThreeTimes(String[] words) {
        System.out.println("\nRemove words that occur exactly three times and print rest: ");
        Map<String, Integer> m = getMapWithWordsAndOccurrences(words);
        m.entrySet().stream()
                .filter(e -> e.getValue() != 3)
                .forEach(e -> System.out.println(e.getKey()));
    }

    /**
     * Prints distinct words in lexicographical order, for input String[], after removing words that are longer than
     * 3 letters.
     *
     * @param words input String[]
     */
    private static void removeWordsLongerThanThreeLetters(String[] words) {
        System.out.println("\nDistinct words in lexicographical order, with length less or equal to 3 characters: ");
        Set<String> s = new LinkedHashSet<>(Arrays.asList(words));
        s.stream()
                .filter(w -> w.length() <= 3)
                .forEach(System.out::println);
    }

    /**
     * Returns a Map<String, Integer> for input String[], with keys the distinct words and values the
     * occurrences of each word.
     * Generic function used in many of the functions above, and since in some cases insertion order is needed,
     * LinkedHashSet and LinkedHashMap were chosen as implementations for Sets and Maps accordingly.
     * The setback is a small performance penalty, for the functions where insertion order is not necessary.
     *
     * @param words input String[]
     * @return Map<String, Integer>
     */
    private static Map<String, Integer> getMapWithWordsAndOccurrences(String[] words) {
        List<String> l = Arrays.asList(words);
        Set<String> s = new LinkedHashSet<>(l);
        Map<String, Integer> m = new LinkedHashMap<>();
        s.forEach(w -> m.put(w, Collections.frequency(l, w)));
        return m;
    }
}
