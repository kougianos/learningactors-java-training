package kougianos.day1;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day1 {

    public static void main(String[] args) {
        //for testing purposes
    }

    /**
     * Function that tells whether a given number is odd or even.
     *
     * @param num int
     * @return String
     */
    public static String isNumberOddOrEven(int num) {
        return num % 2 == 0 ? "Even" : "Odd";
    }

    /**
     * Function that returns the factorial of a given number using recursion.
     *
     * @param num int
     * @return int
     */
    public static int factorial(int num) {
        return num == 0 ? 1 : num * factorial(num - 1);
    }

    /**
     * Function that returns the sum of harmonic series of a given number using recursion.
     *
     * @param num int
     * @return int
     */
    public static double harmonic(int num) {
        return num == 1 ? 1.0 : (1.0 / num) + harmonic(num - 1);
    }

    /**
     * Function that returns the quotient (result) of a division, when given a dividend and divisor as input.
     *
     * @param dividend double
     * @param divisor  double
     * @return int
     */
    public static double division(double dividend, double divisor) {

        if (divisor == 0) {
            System.out.print("Division by 0 is not possible.");
            return 0;
        }
        return dividend / divisor;
    }

    /**
     * Function that returns whether a given year is leap or not.
     * In the Gregorian calendar, each leap year has 366 days instead of 365, by extending February to 29 days
     * rather than the common 28. These extra days occur in each year which is an integer multiple of 4
     * (except for years evenly divisible by 100, which are not leap years unless evenly divisible by 400).
     * The leap year of 366 days has 52 weeks and two days, hence the year following a leap year will start later
     * by two days of the week.
     *
     * @param year int
     * @return boolean
     */
    public static boolean isYearLeap(int year) {

        boolean isLeap;

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                isLeap = year % 400 == 0;
            } else
                isLeap = true;
        } else {
            isLeap = false;
        }

        return isLeap;

    }

    /**
     * Function that prints on console all prime numbers until a given number.
     * Eg primeNumbersTill(100) will print all prime numbers from 2 to 100.
     *
     * @param n int
     */
    public static void primeNumbersTill(int n) {
        Stream.concat(Stream.of(2), IntStream.rangeClosed(2, n)
                .filter(Day1::isPrime).boxed())
                .forEach(e -> System.out.print(e + " "));
    }

    /**
     * Private method that checks whether a given number is prime or not, using lambda expression.
     *
     * @param number int
     * @return boolean
     */
    private static boolean isPrime(int number) {
        return Stream.concat(Stream.of(2), IntStream.rangeClosed(2, (int) (Math.sqrt(number)))
                .filter(e -> e % 2 != 0)
                .boxed())
                .noneMatch(n -> number % n == 0);
    }

    /**
     * Given a number n and a divisor k, write the number n in the following representation:n =
     * k*<result of n/k> + modulo. For example for n = 10, k = 7 print“10 = 1*7 + 3”
     *
     * @param dividend int
     * @param divisor  int
     * @return String
     */
    public static String divisionVisualRepresentation(int dividend, int divisor) {
        String result = dividend + " = " + dividend / divisor + "*" + divisor;
        int mod = dividend % divisor;
        return mod == 0 ? result : result + " + " + mod;
    }

}
