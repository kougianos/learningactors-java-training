package kougianos.day6;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * 1. Fix the FavoriteClasses class to use generics for
 * * its three variables favorite1,favorite2,and favorite3,
 * * its constructor,
 * * its three functions to return each variable
 * * as well as fixing r in the main section. -> DONE
 * <p>
 * 2. Then define a variable of the FavoriteClasses class and use "Hello", 67, 6.3 as the arguments for the constructor,
 * and when you define it use your favorite classes/types that go with the three arguments. -> DONE
 * <p>
 * 3. Can we use the Priority enum as a generic? If so please show an example -> DONE
 * <p>
 * 4. Please declare an Enum with custom constructor and use it -> DONE
 */
public class ExerciseGenericsEnums {
    private enum Priority {
        HIGH, MEDIUM, LOW
    }

    /**
     * Popular crypto coins enum with a custom constructor.
     * Answer to exercise 4.
     */
    private enum PopularCryptoCoins {
        BTC("Bitcoin"),
        BUSD("Binance USD"),
        DOGE("Doge"),
        ETH("Ethereum"),
        LTC("Litecoin");

        private final String currencyName;

        PopularCryptoCoins(String currencyName) {
            this.currencyName = currencyName;
        }

        public String getCurrencyName() {
            return currencyName;
        }

        /**
         * Returns a stream of enum values, which provides an easy way to iterate over the given Enum.
         *
         * @return Stream<PopularCryptoCoins>
         */
        public static Stream<PopularCryptoCoins> stream() {
            return Stream.of(PopularCryptoCoins.values());
        }
    }

    private static class FavoriteClasses<T> {
        private final T favorite1;
        private final T favorite2;
        private final T favorite3;

        FavoriteClasses(T fav1, T fav2, T fav3) {
            this.favorite1 = fav1;
            this.favorite2 = fav2;
            this.favorite3 = fav3;
        }

        public T getFav1() {
            return (this.favorite1);
        }

        public T getFav2() {
            return (this.favorite2);
        }

        public T getFav3() {
            return (this.favorite3);
        }

    }

    public static void main(String[] args) {
        // r.add(6.3) and r.add(5.9) indicates that the List should contain Double. There is no need to use a
        // more generic type (such as Number or Object).
        List<Double> r = new ArrayList<>();
        r.add(6.3);
        r.add(5.9);

        // Here, we see that in the constructor of FavoriteClasses, three different types of objects are used:
        // "Hello" -> String
        // 67 -> Integer
        // r.get(0) -> 6.3 -> Double
        // Hence, we should create a FavoriteClasses variable that accepts a generic Object type.
        FavoriteClasses<Object> a = new FavoriteClasses<>("Hello", 67, r.get(0));
        System.out.println("My favorites are " + a.getFav1() + ", " + a.getFav2() + ", and " + a.getFav3() + ".");


        // Answer to exercise 3. As depicted below, we can use Priority enum as a generic, since FavoriteClasses
        // constructor accepts generic types for all of its 3 fields.
        FavoriteClasses<Priority> p = new FavoriteClasses<>(Priority.LOW, Priority.MEDIUM,
                Priority.HIGH);
        System.out.println("priorityFavoriteClasses contains:: " + p.getFav1() + ", " + p.getFav2() +
                ", and " + p.getFav3() + ".\n");


        // Answer to exercise 4.
        PopularCryptoCoins.stream().forEach(e ->
                System.out.println(e + " coin currency name: " + e.getCurrencyName())
        );

    }
}
