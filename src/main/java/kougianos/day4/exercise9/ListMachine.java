package kougianos.day4.exercise9;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListMachine {

    private List<Integer> numbers = new ArrayList<>();
    private Integer minNumber;

    public List<Integer> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<Integer> numbers) {
        this.numbers = numbers;
    }

    /**
     * User Scanner and get user input in order to fill numbers list.
     */
    public void getUserInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to list machine! Please provide one number at a time that will be added to the " +
                "machine's list, or press n to end user's input: ");

        while (true) {
            String s = scanner.next();
            if (s.equals("n")) {
                break;
            }
            try {
                numbers.add(Integer.parseInt(s));
            } catch (NumberFormatException e) {
                System.out.println("Invalid input, program threw NumberFormatException. " + e.getMessage());
            }
        }
    }

    /**
     * Calculate minimum number in a list of integers. Returns the min integer but also sets the minNumber variable
     * of the class.
     */
    public Integer calculateMinNumber(List<Integer> integerList) {

        if (!integerList.isEmpty()) {
            int i = integerList.stream().mapToInt(e -> e).min().getAsInt();
            this.minNumber = i;
            return i;
        } else {
            System.out.println("Empty list! Nothing to calculate here");
            return -1;
        }
    }

    public void printMinNumber() {
        if (this.minNumber != null) {
            System.out.println("Minimum Number:");
            System.out.println(this.minNumber);
        }
    }

}
