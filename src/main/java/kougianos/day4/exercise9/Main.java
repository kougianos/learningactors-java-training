package kougianos.day4.exercise9;

public class Main {

    public static void main(String[] args) {
        ListMachine listMachine = new ListMachine();

        listMachine.getUserInput();
        listMachine.calculateMinNumber(listMachine.getNumbers());
        listMachine.printMinNumber();
    }
}
