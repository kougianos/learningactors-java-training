package kougianos.day4.exercise8;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class ArrayMachine {

    private Integer[] numbers = new Integer[0];

    public Integer[] getNumbers() {
        return numbers;
    }

    public void setNumbers(Integer[] numbers) {
        this.numbers = numbers;
    }

    /**
     * User Scanner and get user input in order to fill numbers array.
     */
    public void getUserInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to array machine! Please provide a comma or space separated line with integers\n" +
                "Eg:\n5,3,7,9,3,2,8\nor\n5 3 7 9 3 2 8");
        String input = scanner.nextLine();

        if (!isUserInputValid(input)) {
            System.out.println("Invalid input. Please use one of the 2 formats mentioned at the start of the program.");
            return;
        }

        String[] numbersAsStrings = input.contains(" ") ? input.split(" ") : input.split(",");
        try {
            this.numbers = Arrays.stream(numbersAsStrings).mapToInt(Integer::parseInt).boxed().toArray(Integer[]::new);
        } catch (NumberFormatException e) {
            System.out.println("Invalid input, program threw NumberFormatException. " + e.getMessage());
        }

    }

    /**
     * Sort numbers array in descending order.
     */
    public void sortNumbersDescending() {

        if (this.numbers.length > 0) {
            Arrays.sort(this.numbers, Collections.reverseOrder());
        } else {
            System.out.println("Empty numbers array, nothing to sort here :)");
        }

    }

    /**
     * Print numbers array.
     */
    public void printNumbers() {
        System.out.println("Output: ");
        System.out.println(Arrays.toString(numbers));
    }

    private boolean isUserInputValid(String input) {
        return !(input.contains(" ") && input.contains(","));
    }

}
