package kougianos.day4.exercise8;

public class Main {

    public static void main(String[] args) {
        ArrayMachine machine = new ArrayMachine();

        machine.getUserInput();
        machine.sortNumbersDescending();
        machine.printNumbers();
    }
}
